text = input(
    "Ingrese un texto: "
).lower().strip()

words = text.split()

q_words = len(words)

first_letter = text[0]
last_letter = text[-1]

q_letters = {}

# text inverso
reverse_text = " ".join(
    reversed(words)
)

# Validar ingreso de 3 letras a ver su repeticion
while True:
    letters = input("Ingrese 3 letras para ver su cantidad: ")
    if len(letters) == 3:
        break
    print("Debe ingresar exactamente 3 letras.")


# Cantidad de veces que aparece una letra
for letter in letters:
    q_letters[letter] = text.count(letter)

print(
    "Aparicienes de letras :"
)

for letter, cant in q_letters.items():
    print(
        f"{letter} -> {cant}"
    )

# Cantidad de palabras

print(
    f"Cantidad de palabras en el texto -> {q_words}"
)

# Primer letra ultima

print(
    f"""La primer letra es -> {first_letter}
La ultima letra es -> {last_letter}
"""
)

print(
    f"""El texto al reves ingresado :
{reverse_text}
"""
)

# Validar que aparesca la palabra "pyrhon" en el texto

if "python" in text:
    print(
        "La palabra 'python' aparece en el texto!!!"
    )
else:
    print(
        "La palabra 'python' no aparece en el texto"
    )
