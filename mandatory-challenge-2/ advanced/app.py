from helpers.index import contar_letras, contar_palabras,obtener_primera_ultima_letra, invertir_texto, comprobar_palabra_python

texto = input("Ingrese un texto: ")
letras_a_contar = input("Ingrese 3 letras: ")

# Cantidad de veces que aparece cada letra
contador_letras = contar_letras(texto, letras_a_contar)
print("Cantidad de veces que aparece cada letra:")
for letra, cantidad in contador_letras.items():
    print(f"{letra}: {cantidad}")

# Cantidad de palabras en el texto
cantidad_palabras = contar_palabras(texto)
print(f"Cantidad de palabras en el texto: {cantidad_palabras}")

# Primera letra y última letra
primera_letra, ultima_letra = obtener_primera_ultima_letra(texto)
print(f"Primera letra: {primera_letra}")
print(f"Última letra: {ultima_letra}")

# Texto en orden inverso
texto_inverso = invertir_texto(texto)
print(f"Texto en orden inverso: {texto_inverso}")

# La palabra "python" aparece en el texto?
mensaje_palabra_python = comprobar_palabra_python(texto)
print(mensaje_palabra_python)
