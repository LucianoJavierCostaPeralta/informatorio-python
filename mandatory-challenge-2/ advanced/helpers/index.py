def contar_letras(texto, letras_a_contar):
    """Cuenta la cantidad de veces que aparece cada letra en el texto"""
    letras_a_contar = letras_a_contar.lower()
    contador_letras = {}
    for letra in letras_a_contar:
        contador_letras[letra] = texto.lower().count(letra)
    return contador_letras


def contar_palabras(texto):
    """Cuenta la cantidad de palabras en el texto"""
    palabras = texto.split()
    cantidad_palabras = len(palabras)
    return cantidad_palabras


def obtener_primera_ultima_letra(texto):
    """Obtiene la primera y última letra del texto"""
    primera_letra = texto.strip()[0]
    ultima_letra = texto.strip()[-1]
    return primera_letra, ultima_letra


def invertir_texto(texto):
    """Invierte el orden del texto"""
    texto_inverso = texto[::-1]
    return texto_inverso


def comprobar_palabra_python(texto):
    """Comprueba si la palabra 'python' aparece en el texto"""
    palabras = texto.split()
    palabra_python = "python" in palabras
    mensaje = {
        True: "La palabra python aparece en el texto.",
        False: "La palabra python no aparece en el texto."
    }
    return mensaje[palabra_python]
