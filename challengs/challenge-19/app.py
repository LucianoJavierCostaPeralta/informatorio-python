numero = float(
    input(
        "Ingrese un número decimal: "
    )
)

parte_entera = int(numero)

parte_decimal = numero - parte_entera

print(
    f"""
Numero ingresado en decimales -> {numero}
Parte entera del numero ingresado -> {parte_entera}
Parte decimal del numero ingresado -> {parte_decimal}
"""
)
