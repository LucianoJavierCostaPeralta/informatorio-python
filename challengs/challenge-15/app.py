celsius = float(input("Ingrese una temperatura en grados Celsius: "))

fahrenheit = (celsius * 1.8) + 32

print(celsius, "grados Celsius equivalen a", fahrenheit, "grados Fahrenheit.")
print(
    f"""
Grados celsius ingresados : {celsius}
Grados fahrenheit : {fahrenheit}
"""
)