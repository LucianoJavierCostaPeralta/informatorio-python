import random

nombre = input("Ingresa tu nombre: ").strip().capitalize()
print("Hola, " + nombre + "! Estoy pensando en un número entre 1 y 100.")
print("Tienes 8 intentos para adivinarlo.")

numero_secreto = random.randint(1, 100)
intentos_restantes = 8

while intentos_restantes > 0:
    print("Intentos restantes:", intentos_restantes)
    numero_ingresado = input("Ingresa un número: ")

    if not numero_ingresado.isnumeric():
        print("No has ingresado un número entero. Intenta de nuevo.")
        continue

    numero_ingresado = int(numero_ingresado)

    if numero_ingresado < numero_secreto:
        print("El número a adivinar es mayor.")
    elif numero_ingresado > numero_secreto:
        print("El número a adivinar es menor.")
    else:
        intento_actual = 9 - intentos_restantes
        print("¡Felicidades, has ganado!")
        print("Adivinaste el número en el intento número", intento_actual)
        break

    intentos_restantes -= 1
    print()

if intentos_restantes == 0:
    print("Se te han agotado los intentos. El número que debías adivinar era:",
        numero_secreto)
